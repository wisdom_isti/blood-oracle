<?php
    include('../../connection.php');

    $condition = '';
    $hn =$_GET['hn'];

    $sql = "SELECT 	RC.* ,
                UO.unitofficename,
                PT.patientfullname,
                BG.bloodgroupname,
                RH.rhname3
                
            FROM bb_blood_washed_red_cell RC
            LEFT JOIN bb_patient PT ON RC.patientid = PT.patientid
            LEFT JOIN bb_unit_office UO ON RC.unitofficeid = UO.unitofficeid
            LEFT JOIN bb_blood_group BG ON RC.bloodgroupid = BG.bloodgroupid
            LEFT JOIN bb_rh RH ON RC.rhid = RH.rhid
            WHERE RC.active <> 0
            AND PT.patienthn = '$hn'
            ORDER BY RC.bloodwashedredcellid DESC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>