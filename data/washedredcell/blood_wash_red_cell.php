<?php
    include('../../connection.php');

    $condition = '';
    $requestbloodid =$_GET['requestbloodid'];


    $sql = "SELECT 
    DATE_FORMAT(STR_TO_DATE(BWR.blood_wash_use_date, '%Y-%m-%d'),'%d/%m/%Y') AS blood_wash_use_date_2 ,
    DOC.doctorname,
    OFF.unitofficename,
    RH.rhname3,
    BWR.*
    FROM bb_blood_washed_red_cell BWR
    LEFT JOIN bb_doctor DOC ON DOC.doctorid = BWR.user_order
    LEFT JOIN bb_unit_office OFF ON OFF.unitofficeid = BWR.unitofficeid
    LEFT JOIN bb_rh RH ON RH.rhid = BWR.rhid
    WHERE BWR.requestbloodid = '$requestbloodid' ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
    // $resultArray =[$hn, $requestbloodid];
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
