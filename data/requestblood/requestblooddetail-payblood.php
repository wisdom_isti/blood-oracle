<?php
    include('../../connection.php');

    $id =$_GET['id'];

    $sql = "SELECT
            COUNT(CM.\"ispayblood\") AS \"QTY\" ,
            -- CM.* ,
            CM.\"bloodtype\",
            CM.\"requestbloodid\",
            RH.\"rhname3\",
            BT.\"bloodstocktypename2\"
            FROM \"bb_request_blood_crossmacth\" CM
            LEFT JOIN \"bb_rh\"  RH ON CM.\"rhid\" = RH.\"rhid\"
            LEFT JOIN \"bb_bloodstock_type\" BT ON CM.\"bloodtype\" = BT.\"bloodstocktypeid\" 
            WHERE  CM.\"requestbloodid\" = '$id'
            AND CM.\"ispayblood\" = '1'
            GROUP BY 
            CM.\"bloodtype\",
            CM.\"requestbloodid\",
            CM.\"ispayblood\",
            RH.\"rhname3\",
            BT.\"bloodstocktypename2\"
            ORDER BY CM.\"requestbloodid\"
";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);

?>