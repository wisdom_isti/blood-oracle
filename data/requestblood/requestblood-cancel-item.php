<?php
    include('../../connection.php');

    $id =$_GET['id'];
    $requestbloodid =$_GET['requestbloodid'];

    $sql = "SELECT 
    CN.\"requestbloodcancelname\" AS \"grouprequestbloodcancelname\",
    IM.* ,
    RB.\"requestbloodcancelother\"
    FROM \"bb_request_blood\" RB
    LEFT JOIN \"bb_request_blood_cancel_item\" IM ON RB.\"requestbloodid\" = IM.\"requestbloodid\"
    LEFT JOIN \"bb_request_blood_cancel\" CN ON IM.\"requestbloodcancelid\" = CN.\"requestbloodcancelid\"
    WHERE RB.\"requestbloodid\" = '$requestbloodid' 
    ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);

?>