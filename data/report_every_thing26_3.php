<?php
    include('../connection.php');

    $condition = '';
    $fromdatetime =$_GET['fromdatetime'];
    $todatetime =$_GET['todatetime'];
    $usercreate =$_GET['usercreate'];

    if(!empty($usercreate))
    $condition = " AND RC.usercreate = '$usercreate' ";

    $sql = "SELECT 	
    ROW_NUMBER() OVER (ORDER By RC.bloodwashedredcellid DESC) AS num_row ,
    RC.* ,
                UO.unitofficename,
                PT.patientfullname,
                PT.patienthn,
                BG.bloodgroupname,
                RH.rhname3,
                CONCAT(S.name,' ',S.surname) AS usercreatename,
				DATE_FORMAT(RC.user_send_wash_date,'%d/%m/%Y %H:%I') AS user_send_wash_date
                
            FROM bb_blood_washed_red_cell RC
            LEFT JOIN bb_patient PT ON RC.patientid = PT.patientid
            LEFT JOIN bb_unit_office UO ON RC.unitofficeid = UO.unitofficeid
            LEFT JOIN bb_blood_group BG ON RC.bloodgroupid = BG.bloodgroupid
            LEFT JOIN bb_rh RH ON RC.rhid = RH.rhid
            LEFT JOIN bb_staff S ON RC.usercreate = S.id
            WHERE RC.active <> 0
            AND RC.user_send_wash_date BETWEEN '$fromdatetime' AND '$todatetime'
            $condition
            ORDER BY RC.bloodwashedredcellid DESC";


    error_log($sql);
 
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'sql' => $sql
        )
        
    );

    oci_close($conn);
?>