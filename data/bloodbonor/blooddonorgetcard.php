<?php
    include('../../connection.php');

    $condition = '';
    $donorid =$_GET['donorid'];
  
    $sql = "SELECT DN.* ,
            SF.\"name\",
            SF.\"surname\"
            FROM \"bb_donate\" DN
            LEFT JOIN \"bb_staff\" SF ON DN.\"staffcardid\" = SF.id
            WHERE DN.\"getcard\" = 1
            AND \"donorid\" = '$donorid'
            ORDER BY DN.\"getcarddate\" DESC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>