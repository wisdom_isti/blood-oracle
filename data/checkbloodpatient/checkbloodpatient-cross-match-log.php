<?php
    include('../../connection.php');

    $condition = '';
    $id =$_GET['requestbloodcrossmacthlogmainid'];
   

    $sql = "SELECT LO.* ,
            RT.\"bloodgroupserumname\" AS \"bloodgroupserumname_rt\",
            RT.\"bloodgroupserumname\" AS \"bloodgroupserumname_37c\",
            IAT.\"bloodgroupserumname\" AS \"bloodgroupserumname_iat\",
            CCC.\"bloodgroupserumname\" AS \"bloodgroupserumname_ccc\",
            CAT.\"bloodgroupserumname\" AS \"bloodgroupserumname_cat\",
            RH.\"rhcode\",
            CR.\"crossmacthresultname\",
            ST.\"crossmacthstatusname\",
            DT.\"doctorname\",
            SF.\"gender\" || SF.\"name\" || ' ' || SF.\"surname\" AS \"staff_name\"
            FROM \"bb_request_blood_crossmacth_log\" LO
            LEFT JOIN \"bb_blood_group_serum_crossmacth\" RT ON LO.\"ctt_rt\" = RT.\"bloodgroupserumid\"
            LEFT JOIN \"bb_blood_group_serum_crossmacth\" C37 ON LO.\"ctt_37c\" = C37.\"bloodgroupserumid\"
            LEFT JOIN \"bb_blood_group_serum_crossmacth\" IAT ON LO.\"ctt_iat\" = IAT.\"bloodgroupserumid\"
            LEFT JOIN \"bb_blood_group_serum_crossmacth\" CCC ON LO.\"ctt_ccc\" = CCC.\"bloodgroupserumid\"
            LEFT JOIN \"bb_blood_group_serum_crossmacth\" CAT ON LO.\"ctt_ccc\" = CAT.\"bloodgroupserumid\"
            LEFT JOIN \"bb_crossmacth_result\" CR ON LO.\"crossmacthresultid\" = CR.\"crossmacthresultid\"
            LEFT JOIN \"bb_crossmacth_status\" ST ON LO.\"crossmacthstatusid\" = ST.\"crossmacthstatusid\"
            LEFT JOIN \"bb_rh\" RH ON LO.\"rhid\" = RH.\"rhid\"
            LEFT JOIN \"bb_doctor\" DT ON LO.\"doctorid\" = DT.\"doctorid\"
            LEFT JOIN \"bb_staff\" SF ON LO.\"isbloodpreparation\" = SF.\"id\"
            WHERE LO.\"requestbloodcrossmacthlogmainid\" = '$id'
            ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    error_log($sql);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>