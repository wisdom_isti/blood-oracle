<?php
    include('../../connection.php');

    $condition = '';
    $id =$_GET['id'];


    // $sql = "SELECT LC.labcheckrequest_a_user , LC.labcheckrequest_v_user ,
    // IM.* ,LM.labformname,LM.labformcode 
    //         FROM bb_lab_check_request_item IM
    //         LEFT JOIN bb_labform LM ON IM.labformid = LM.labformid
    //         LEFT JOIN bb_lab_check_request LC ON LC.labcheckrequestid = IM.labcheckrequestid
    //         WHERE IM.active <> 0
    // AND IM.labcheckrequestid = '$id'
    // GROUP BY LM.labformname,LM.labformcode";

    $sql = "SELECT 
            IM.* ,
            LC.\"labcheckrequest_a_user\" ,
            LC.\"labcheckrequest_v_user\" ,
            LM.\"labformname\",
            LM.\"labformcode\" 
            FROM \"bb_lab_check_request_item\" IM
            LEFT JOIN \"bb_labform\" LM ON IM.\"labformid\" = LM.\"labformid\"
            LEFT JOIN \"bb_lab_check_request\" LC ON LC.\"labcheckrequestid\" = IM.\"labcheckrequestid\"
            WHERE IM.\"active\" <> 0
            AND IM.\"labcheckrequestid\" = '$id'
            GROUP BY LM.\"labformname\",
            LM.\"labformcode\",
            LC.\"labcheckrequest_a_user\" ,
            LC.\"labcheckrequest_v_user\" ,
            IM.\"labcheckrequestitemid\",
            IM.\"labcheckrequestid\",
            IM.\"labcheckrequestitem_v\",
            IM.\"labcheckrequestitem_a\",
            IM.\"labformid\",
            IM.\"labcheckrequestitemprice\",
            IM.\"labcheckrequestitemwiden\",
            IM.\"labcheckresult\",
            IM.\"labcheckresultshow\",
            IM.\"labchecknormal\",
            IM.\"labcheckvalue\",
            IM.\"labcheckunit\",
            IM.\"active\",
            IM.\"lab_user_a\",
            IM.\"lab_user_v\",
            IM.\"a_date_time\",
            IM.\"v_date_time\",
            IM.\"sendapi_status\",
            IM.\"item_remark\",
            IM.\"labcheckcommentanalyze\",
            IM.\"sendapi_result\",
            IM.\"sendapi_data\"
    ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>