<?php
    include('../../connection.php');

    $condition = '';
    $hn =$_GET['hn'];

    // $sql = "SELECT 	BEX.* ,
    //             PT.patientfullname,
    //             PT.patientan,
    //             BG.bloodgroupname,
    //             RH.rhname3,
    //             DT.doctorname,
    //             MAC.exchangemachinename,
    //             TY.bloodexchangetypename,
    //             UF.unitofficename
                
    //         FROM bb_blood_exchange BEX
    //         LEFT JOIN bb_patient PT ON BEX.patientid = PT.patientid
    //         LEFT JOIN bb_blood_group BG ON BEX.bloodgroupid = BG.bloodgroupid
    //         LEFT JOIN bb_unit_office UF ON BEX.unitofficeid = UF.unitofficeid
    //         LEFT JOIN bb_rh RH ON BEX.rhid = RH.rhid
    //         LEFT JOIN bb_doctor DT ON BEX.doctorid = DT.doctorid
    //         LEFT JOIN bb_blood_exchange_machine MAC ON BEX.exchangemachineid = MAC.exchangemachineid
    //         LEFT JOIN bb_blood_exchange_type TY ON BEX.bloodexchangetypeid = TY.bloodexchangetypeid
    //         WHERE BEX.active <> 0
    //         AND PT.patienthn = '$hn'
    //         ORDER BY BEX.bloodexchangeid DESC";

            $sql = "SELECT
                    RB.\"requestunit\" ,
                    UN.\"unitofficename\",
                    RB.\"requestblooddate\",
                    RB.\"requestbloodtime\",
                    RBC.\"wash_status\" ,
                    RBC.\"wash_status_remark\",
                    DOC.\"doctorid\" ,
                    DOC.\"doctorname\",
                    RB.\"diagnosis\" ,
                    RB.\"diagnosisdetail\",
                    RB.\"requestbloodid\",
                    RB.\"hn\",
                    PA.\"patientid\",
                    RBC.\"bag_number\",
                    RBC.\"bloodgroupid\",
                    RBC.\"rhid\",
                    RH.\"rhname3\",
                    RBC.\"bloodtype\",
                    BS.\"volume\",
                    RBC.\"crossmacthstatusid\",
                    CS.\"crossmacthstatusname\",
                    RBC.\"requestbloodcrossmacthdatetime\",
                    RBC.\"isbloodpreparation\",
                    SS.\"name\" ,
                    SS.\"surname\"
                    FROM \"bb_request_blood_crossmacth\" RBC
                    JOIN \"bb_request_blood\" RB ON RB.\"requestbloodid\" = RBC.\"requestbloodid\"
                    JOIN \"bb_bloodstock\" BS ON BS.\"bag_number\" = RBC.\"bag_number\"
                    JOIN \"bb_staff\" SS ON SS.\"id\" = RBC.\"isbloodpreparation\"
                    JOIN \"bb_crossmacth_status\" CS ON CS.\"crossmacthstatusid\" = RBC.\"crossmacthstatusid\"
                    JOIN \"bb_rh\" RH ON RH.\"rhid\" = RBC.\"rhid\"
                    JOIN \"bb_patient\" PA ON PA.\"patienthn\" = RB.\"hn\"
                    JOIN \"bb_doctor\" DOC ON DOC.\"doctorid\" = RB.\"doctorid\"
                    JOIN \"bb_unit_office\" UN ON UN.\"unitofficeid\" = RB.\"requestunit\"
                    WHERE RB.\"hn\" = '$hn'
                    AND RBC.\"confirmbloodrequestdate\" = (SELECT TO_CHAR(SYSDATE,'YYYY-MM-DD','NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') FROM DUAL)
                    AND RBC.\"bloodtype\" IN('FFP','LPPC','LPPC(PAS)','SDP','SDP(PAS)','LDPPC','LDPPC_PAS','CRP')
                    ORDER BY RBC.\"bag_number\"
            ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>