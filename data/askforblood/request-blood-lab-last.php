<?php
    include('../../connection.php');
    date_default_timezone_set('Asia/Bangkok');
    include('../../dateNow.php');

    $condition = '';
    $hn =$_GET['hn'];

    

    $sql = "SELECT * FROM (SELECT 
                    RB.* ,
                    BG.\"bloodgroupname\",
                    RH.\"rhname3\",
                    SC.\"rhname3\" AS \"confirmsceenname\",
                    REPLACE(RB.\"phenotypedisplay\",' ','') AS \"group_phenotypedisplay_all\",
                    (SELECT listagg(DISTINCT \"antibody\",', ') FROM \"bb_request_blood\"  WHERE \"hn\" = '$hn' ) AS \"group_antibody_all\"
                    FROM \"bb_request_blood\" RB 
                    LEFT JOIN \"bb_blood_group\" BG ON RB.\"confirmbloodgroup\" = BG.\"bloodgroupid\"
                    LEFT JOIN \"bb_rh\" RH ON RB.\"confirmrhid\" = RH.\"rhid\"
                    LEFT JOIN \"bb_rh\" SC ON RB.\"confirmsceen\" = SC.\"rhid\"
                    WHERE RB.\"hn\" = '$hn' 
                    AND RB.\"iscrossmatch\" = 1
                    AND RB.\"antibody\" IS NOT NULL
                    ORDER BY RB.\"requestbloodid\" DESC
                    ) WHERE rownum <= 1
                    " ;

    $query = oci_parse($conn,$sql);
    oci_execute($query);


    $resultArray = array();
   
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
    }
    

    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'total' => count($resultArray)
        )
        
    );

    oci_close($conn);
?>