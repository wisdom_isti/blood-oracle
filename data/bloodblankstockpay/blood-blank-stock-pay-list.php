<?php
    include('../../connection.php');
    include('../dateFormat.php');

    $condition = '';
    $fromdate = $_GET['fromdate'];
    $todate = $_GET['todate'];

    $bloodstockpaytypeid = $_GET['bloodstockpaytypeid'];
    $hospital_pay = $_GET['hospital_pay'];

    if(!empty($bloodstockpaytypeid) && $bloodstockpaytypeid != "null")
    $condition = $condition." AND MN.\"bloodstockpaytypeid\" = '$bloodstockpaytypeid' ";

    if(!empty($hospital_pay) && $hospital_pay != "null")
    $condition = $condition." AND MN.\"hospitalid\" = '$hospital_pay' ";

    $sql = "SELECT
            LISTAGG(PY.\"bloodgroup\" , '<br />') AS \"bloodgroup\",
            LISTAGG(PY.\"bloodtype\" , '<br />') AS \"bloodtype_bag_number\",
            LISTAGG(PY.\"bag_number\" , '<br />' ON OVERFLOW TRUNCATE '!!!') 
            WITHIN GROUP(ORDER BY PY.\"bag_number\") AS \"group_bag_number\" ,
            LISTAGG(URC.\"fullname\" , '<br />' ) AS \"fullname\" ,

            CASE WHEN MN.\"bloodstockpaytypeid\" = 8 THEN ' ' || MN.\"hn_pay_out\" || MN.\"patient_pay_out\" || '<br>' || TO_CHAR(MN.\"patient_pay_date\",'DD/MM/YYYY') || MN.\"patient_pay_time\" || MN.\"bloodstockpaymainremark\"
            WHEN MN.\"bloodstockpaytypeid\" = 9 THEN ' ' || BRO.\"bloodbrokenname\" || MN.\"bloodstockpaymainremark\"
            ELSE MN.\"bloodstockpaymainremark\" END AS \"bloodstockpaymainremark\",

            MN.* ,
            HP.\"hospitalname\",
            PT.\"bloodstockpaytypename\"

            FROM \"bb_bloodstock_pay_main\" MN
            LEFT JOIN \"bb_bloodstock_pay\" PY ON MN.\"bloodstockpaymainid\" = PY.\"bloodstockpaymainid\"
            LEFT JOIN \"bb_hospital\" HP ON MN.\"hospitalid\" = HP.\"hospitalid\"
            LEFT JOIN \"bb_bloodstock_pay_type\" PT ON MN.\"bloodstockpaytypeid\" = PT.\"bloodstockpaytypeid\"
            LEFT JOIN \"bb_users\" URC ON URC.\"username\" = MN.\"bloodstockpaymainuser\"
            LEFT JOIN \"bb_blood_broken\" BRO ON BRO.\"bloodbrokenid\" = MN.\"bloodbrokenid\"
            WHERE MN.\"active\" <> 0
                AND MN.\"bloodstockpaymaindate\" BETWEEN TO_DATE ('$fromdate', 'YYYY-mm-dd')
                AND TO_DATE('$todate', 'YYYY-mm-dd')
                $condition
                GROUP BY MN.\"bloodstockpaymainid\",
                MN.\"bloodstockpaymaincode\",
                MN.\"bloodstockpaymaindate\",
                MN.\"bloodborrowid\",
                MN.\"bloodstockpaymainuser\",
                MN.\"bloodstockpaytypeid\",
                MN.\"hospitalid\",
                MN.\"hn_pay_out\",
                MN.\"patient_pay_out\",
                MN.\"active\",
                MN.\"bloodbrokenid\",
                MN.\"bloodstockpaymainremark2\",
                MN.\"patient_pay_date\",
                MN.\"patient_pay_time\",
                MN.\"bloodstockpaymainremark\",
                HP.\"hospitalname\",
                PT.\"bloodstockpaytypename\",
                BRO.\"bloodbrokenname\"
                ORDER BY MN.\"bloodstockpaymainid\" DESC
    
            ";
    
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    error_log($sql);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'total' => count($resultArray)
        )
        
    );

    oci_close($conn);
?>