<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    $hn =$_GET['hn'];
    $donoridcard =$_GET['donoridcard'];

    // if(!empty($keyword))
    // $condition = $condition." AND CONCAT(   ifnull(DN.fname,''),' ',
    //                             ifnull(DN.lname,''),' ',
    //                             ifnull(DN.donoridcard,''),' ',
    //                             ifnull(DN.donorpassport,''),' ',
    //                             ifnull(DN.donorcode,'')) LIKE '%$keyword%' ";

    
    // if(!empty($hn))
    // $condition = $condition." AND REPLACE(DN.donorcode,'-','') = REPLACE('$hn','-','') ";

    // if(!empty($donoridcard))
    // $condition = $condition." AND DN.donoridcard = '$donoridcard' ";

    if(!empty($donoridcard))
    $keyword = $donoridcard;

    // $sql = "SELECT MAX(DT.donateid),DT.bag_number,DT.donation_status,DN.* , 
    //             BG.bloodgroupname , 
    //             IFNULL(RH.rhname3,'') AS rhname3, 
    //             SRH.rhname3 AS antibodyscreeningname
    //         FROM bb_donor DN
    //         LEFT JOIN bb_blood_group BG ON DN.blood_group = BG.bloodgroupid
    //         LEFT JOIN bb_rh RH ON DN.rh = RH.rhid
    //         LEFT JOIN bb_rh SRH ON DN.antibodysceening = SRH.rhid
    //         LEFT JOIN bb_(SELECT * FROM bb_donate ORDER BY donation_date DESC) DT ON DN.donorid = DT.donorid
    //         WHERE true
    //         $condition 
    //         GROUP BY DT.donorid
    //         ORDER BY DT.donateid DESC,DT.donation_date DESC,DN.fname DESC		
    //         LIMIT 50";

    $resultArray = array();

    if(!empty($keyword))
    {
        $sql = "SELECT
(SELECT TO_CHAR(SYSDATE,'YYYY-MM-DD','NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') FROM DUAL) AS \"DATE_NOW\" ,
SUBSTR(LISTAGG(DT.\"donation_type_id\", ','), '', 1)  AS \"donation_type_id\" ,
MAX(DT.\"donation_date\") AS \"donation_date\",
MAX(DT.\"donation_date\") AS \"max_donation_date\",
DN.*,
DT.\"donateid\",
DT.\"bag_number\",
DT.\"donation_status\",
PF.\"prefixname\",
BG.\"bloodgroupname\",
NVL(RH.\"rhname3\", '' ) AS \"rhname3\",
SRH.\"rhname3\" AS \"antibodyscreeningname\"
FROM \"bb_donor\" DN
LEFT JOIN \"bb_donate\" DT ON DN.\"donorid\" = DT.\"donorid\"
LEFT JOIN \"bb_blood_group\" BG ON DN.\"blood_group\" = BG.\"bloodgroupid\"
LEFT JOIN \"bb_rh\" RH ON DN.\"rh\" = RH.\"rhid\"
LEFT JOIN \"bb_rh\" SRH ON DN.\"antibodysceening\" = SRH.\"rhid\"
LEFT JOIN \"bb_prefix\" PF ON DN.\"prefixid\" = PF.\"prefixid\"
WHERE 1=1
AND ROWNUM <= 50
AND NVL(DN.\"fname\",'') || ' ' || NVL(DN.\"lname\",'') || ' ' || NVL(DN.\"donoridcard\",'') || ' ' ||NVL(DN.\"donorpassport\",'') || ' ' || NVL(DN.\"donorcode\",'') LIKE '%$keyword%' 
GROUP BY 
DN.\"donorid\",
DN.\"donorcode\",
DN.\"isidcardpassport\",
DN.\"donoridcard\",
DN.\"donorpassport\",
DN.\"donorbirthday\",
DN.\"donorage\",
DN.\"donoragetext\",
DN.\"donoroccupation\",
DN.\"donoroccupationother\",
DN.\"donortelhome\",
DN.\"donormobile\",
DN.\"genderid\",
DN.\"prefixid\",
DN.\"fname\",
DN.\"lname\",
DN.\"address_moo\",
DN.\"address_alley\",
DN.\"address_street\",
DN.\"countryid\",
DN.\"provinceid\",
DN.\"districtid\",
DN.\"subdistrictid\",
DN.\"zipcode\",
DN.\"address_current\",
DN.\"address_moo_current\",
DN.\"address_alley_current\",
DN.\"address_street_current\",
DN.\"address2_current\",
DN.\"countrycurrentid\",
DN.\"provincecurrentid\",
DN.\"districtcurrentid\",
DN.\"subdistrictcurrentid\",
DN.\"zipcode_current\",
DN.\"issendletter\",
DN.\"souvenirid\",
DN.\"blood_group\",
DN.\"rh\",
DN.\"lastdate\",
DN.\"lasttime\",
DN.\"donoremail\",
DN.\"antibodysceening\",
DN.\"donation_type_id_last\",
DN.\"ischangename\",
DN.\"ischangepassport\",
DN.\"lastcheckdate\",
DN.\"lastchecktime\",
DN.\"apiupdate\",
DN.\"donatefirstupdate\",
DN.\"donor_isunitoffice\",
DN.\"donor_departmentid\",
DN.\"address\",
DN.\"address2\",
DN.\"antibody\",
DN.\"phenotype\",
DN.\"phenotypeshow\",
DN.\"donorimagepath\",
DT.\"donateid\" ,
DT.\"bag_number\",
DT.\"donation_status\",
PF.\"prefixname\",
BG.\"bloodgroupname\",
DT.\"donation_date\",
RH.\"rhname3\",
SRH.\"rhname3\"
ORDER BY DT.\"donateid\" DESC,DT.\"donation_date\" DESC,DN.\"fname\" DESC
        ";

        $query = oci_parse($conn,$sql);
    oci_execute($query);
        while($result = oci_fetch_array($query))
        {
            array_push($resultArray,$result);
        }
    }

    
    error_log($sql);
  
    
	
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>