<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"machineid\",'') || ' ' || 
                        NVL(\"machinecode\",'') || ' ' ||
                        NVL(\"machinename\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_machine\" where \"active\" <> 0 $condition ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>