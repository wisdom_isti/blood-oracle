<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"hospitalid\",'') || ' ' || NVL(\"hospitalcode\",'') || ' ' || NVL(\"hospitalname\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_hospital\" 
            where \"active\" <> 0 $condition  ORDER BY \"hospitalid\" ASC ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>