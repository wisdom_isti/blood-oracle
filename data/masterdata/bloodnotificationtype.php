<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    $requestunit =$_GET['requestunit'];
    

    if(!empty($keyword))
    $condition = $condition." AND NVL(\"bloodnotificationtypename\",'') || NVL(\"bloodnotificationtypecode\",'') LIKE '%$keyword%' ";

    if($requestunit != 199)
    $condition = $condition. " AND  \"bloodnotificationtypeid\" NOT IN (2) ";

    $sql = "SELECT * FROM \"bb_blood_notification_type\" 
            WHERE 1=1 $condition  
            ORDER BY \"bloodnotificationtypecode\" ASC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>