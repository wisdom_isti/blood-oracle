<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"labformcode\",'') || ' ' || NVL(\"labformname\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_labform\" where \"active\" <> 0 $condition  ORDER BY \"labsort\"";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>