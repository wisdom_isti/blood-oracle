<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];

    $subdistrictid =$_GET['subdistrictid'];

    $subdistrict =$_GET['subdistrict'];
    $district =$_GET['district'];
    $province =$_GET['province'];
    
    if(!empty($keyword))
    $condition = "AND NVL(prov.\"provinceth\",'') || ' ' || NVL(dis.\"districtth\",'') || ' ' || NVL(sub.\"subdistrictth\",'') ||' ' || NVL(sub.\"zipcode\",'') LIKE '%$keyword%'";

    if(!empty($subdistrictid))
    $condition = $condition." AND sub.\"subdistrictid\" = '$subdistrictid' ";

    if(!empty($subdistrict))
    $condition = $condition." AND sub.\"subdistrictth\" like '%$subdistrict%' ";

    if(!empty($district))
    $condition = $condition." AND dis.\"districtth\" like '%$district%' ";

    if(!empty($province))
    $condition = $condition." AND prov.\"provinceth\" like '%$province%' ";


    $sql = "SELECT sub.*,
    dis.\"districten\",
    dis.\"districtth\",
    prov.\"provinceid\",
    prov.\"provinceen\",
    prov.\"provinceth\",
    CASE WHEN prov.\"provinceid\" = 10000000 then 'แขวง ' else 'ตำบล ' end || 
		NVL(sub.\"subdistrictth\",'') || 
		'   ' || 
		CASE WHEN prov.\"provinceid\" = 10000000 then 'เขต ' else 'อำเภอ ' end || 
		NVL(dis.\"districtth\",'') || 
		'   จังหวัด ' ||
		NVL(prov.\"provinceth\",'') as \"address\"
    FROM \"bb_subdistricts\" sub
    LEFT JOIN \"bb_districts\" dis ON sub.\"districtid\" = dis.\"districtid\"
    LEFT JOIN \"bb_provinces\" prov ON dis.\"provinceid\" = prov.\"provinceid\"
    WHERE ROWNUM <= 50
    $condition
    ORDER BY sub.\"subdistrictth\"";
 
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>