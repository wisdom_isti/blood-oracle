<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"activitycode\",''),' ',NVL(\"activityname\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_donate_activity\" 
            WHERE 1=1 
            AND \"active\" <> 0  
            ORDER BY \"activityid\" DESC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>