<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"occupationid\",'') || ' ' ||
                    NVL(\"occupationcode\",'') || ' ' || 
                    NVL(\"occupationname\",'') LIKE '%$keyword%' ";


    $sql = "SELECT * FROM \"bb_occupation\" 
            WHERE ROWNUM <= 100  
            $condition 
            ORDER BY \"occupationid\"";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>