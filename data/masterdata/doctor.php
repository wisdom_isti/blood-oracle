<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];

    if(!empty($keyword))
    $condition = "AND  NVL(\"doctorname\",'') || ' ' || NVL(\"doctorcode\",'') || ' ' || NVL(\"doctorcode2\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_doctor\" 
            WHERE 1=1 
	        AND ROWNUM <= 50
			ORDER BY \"doctorid\" ASC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>