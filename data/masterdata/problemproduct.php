<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"problemproductid\",'') || ' ' || 
                    NVL(\"problemproductcode\",'') || ' ' || 
                    NVL(\"problemproductname\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_problem_product\" 
            where 1=1 
            $condition 
            ORDER BY \"problemproductid\"";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>