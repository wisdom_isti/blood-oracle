<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];
    if(!empty($keyword))
    $condition = "AND NVL(\"receivingtypeid\",'') || ' ' || NVL(\"receivingtypecode\",'') || ' ' || NVL(\"receivingtypename\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_receiving_type\" 
            WHERE 1=1
            $condition  
            AND \"receivingtypeid\" IN (2,3,4,12)
            ORDER BY \"receivingtypeid\" ASC ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>