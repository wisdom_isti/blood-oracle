<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];

    if(!empty($keyword))
    $condition = "AND NVL(\"bloodbagcode\",'') || ' ' || NVL(\"bloodbagname\",'') LIKE '%$keyword%' ";
    

    $sql = "SELECT * FROM \"bb_blood_bag\" 
            WHERE 1=1 
            $condition 
            ORDER BY \"sort\"";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>