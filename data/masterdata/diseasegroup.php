<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];

    if(!empty($keyword))
    $condition = "AND NVL(\"diseasegroupname\",''),' ',NVL(\"diseasegroupcode\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_disease_group\" 
            WHERE 1=1 
            $condition  
            ORDER BY \"sort\" ASC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>