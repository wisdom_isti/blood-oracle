<?php
    include('../../connection.php');

    $condition = '';
    $keyword =$_GET['keyword'];

    if(!empty($keyword))
    $condition = "AND NVL(\"name\",'') || ' ' || NVL(\"surname\",'') LIKE '%$keyword%' ";

    $sql = "SELECT * FROM \"bb_staff\" WHERE \"isblooddriller\" = 1  ORDER BY \"id\" ASC";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>