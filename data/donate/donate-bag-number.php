<?php
    include('../../connection.php');
    date_default_timezone_set('Asia/Bangkok');
    include('../../dateNow.php');

    $condition = '';
    $bagnumber = $_GET['bagnumber'];


    $resultArray = array();
    if(!empty($bagnumber))
    {
        $sql = "SELECT 
                PF.\"prefixname\",DN.\"fname\"||' '||DN.\"lname\" AS \"fullname\",
                DN.\"donorcode\", 
                DN.\"isidcardpassport\",
                DN.\"donoridcard\",
                DN.\"donorpassport\",
                DT.\"bag_number\"
            FROM \"bb_donate\" DT
            LEFT JOIN \"bb_donor\" DN ON DT.\"donorid\" = DN.\"donorid\"
            LEFT JOIN \"bb_prefix\" PF ON DN.\"prefixid\" = PF.\"prefixid\"
            WHERE DT.\"bag_number\" = '$bagnumber'
            " ;
                
            $query = oci_parse($conn,$sql);
    oci_execute($query);


            while($result = oci_fetch_array($query))
            {
                array_push($resultArray,$result);
            }
    }
    

    echo json_encode(
        array(
        'bagnumber' => $bagnumber,
            'status' => true,
            'data' => $resultArray,
            'total' => count($resultArray)
        )
        
    );

    oci_close($conn);
?>