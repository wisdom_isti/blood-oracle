<?php
    include('../../connection.php');

    $condition = '';
    $condition2 = '';
    $type =$_GET['type'];

    $fromdate =$_GET['fromdate'];
    $todate =$_GET['todate'];

    if(!empty($fromdate) && !empty($todate))
    $condition = $condition."AND DT.\"donation_date\" BETWEEN TO_DATE ('$fromdate', 'YYYY-mm-dd')
			AND TO_DATE('$todate', 'YYYY-mm-dd') ";

    if(strpos($type,"tpharpr") != "")
    {
        $condition2 = $condition2. " OR DT.\"tpharpr\" = '+' ";
    }
    
    if(strpos($type,"hcv") != "")
    {
        $condition2 = $condition2. " OR DT.\"hcvab\" = '+' ";
    }
    
    if(strpos($type,"nat") != "")
    {
        $condition2 = $condition2." OR NVL(DT.hbvdna,'') || NVL(DT.hcvrna,'') || NVL(DT.hivrna,'') LIKE '%+%' ";
    }
    
    if(strpos($type,"hiv") != "")
    {
        $condition2 = $condition2. " OR DT.\"hivagab\" = '+' ";
    }
    
    if(strpos($type,"hbsag") != "")
    {
        $condition2 = $condition2. " OR DT.\"hbsag\" = '+' ";
    }


    $sql = "SELECT
            DT.*,
            DR.\"donorcode\",
            NVL(PF.\"prefixname\",'') || NVL(DR.\"fname\",'') || ' ' || NVL(DR.\"lname\",'') AS fullname,
            PV.\"provinceth\",
            DR.\"donoremail\",
            DR.\"donorage\",
            DR.\"blood_group\"
            FROM \"bb_donate\" DT
            LEFT JOIN \"bb_donor\" DR ON DT.\"donorid\" = DR.\"donorid\"
            LEFT JOIN \"bb_prefix\" PF ON DR.\"prefixid\" = PF.\"prefixid\"
            LEFT JOIN \"bb_provinces\" PV ON DR.\"provinceid\" = PV.\"provinceid\"
            WHERE 1=1
            $condition
            AND ( 1!=1 $condition2 )
            ORDER BY DT.\"donateid\"
            ";
 
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
    }
    

    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'total' => count($resultArray)
        )
        
    );

    oci_close($conn);
?>