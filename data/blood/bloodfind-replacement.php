<?php
    include('../../connection.php');

    $condition = '';
    $hn =$_GET['hn'];

    $donation_get_type_id =$_GET['donation_get_type_id'];
    $sql = "";



    $sql = "SELECT BG.bloodgroupname,M.*,SUM(CASE WHEN IFNULL(M.bloodgroup,'') != '' THEN 1 ELSE 0 END ) AS blood_qty
            FROM bb_blood_group BG
            LEFT JOIN bb_
            (SELECT BS.*,BT.bloodstocktypename2,DN.hn,DN.donation_get_type_id,RH.rhname3,RH.rhcode
            FROM bb_bloodstock BS
            LEFT JOIN bb_donate DN ON BS.donateid = DN.donateid
            LEFT JOIN bb_bloodstock_type BT ON BS.bloodtype = BT.bloodstocktypeid
            LEFT JOIN bb_rh RH ON BS.bloodrh = RH.rhid
            WHERE DN.hn = '$hn'
            AND DN.donation_get_type_id = 2
            AND BS.bloodstockstatusid = 1
            AND BT.bloodstocktypegroupid = 1
            AND BS.active <> 0) M ON BG.bloodgroupid = M.bloodgroup
            WHERE BG.bloodgroupid IN ('A','B','O','AB')
            GROUP BY BG.bloodgroupid
            
            UNION
            
            SELECT BG.bloodgroupname,M.*,SUM(CASE WHEN IFNULL(M.bloodgroup,'') != '' THEN 1 ELSE 0 END ) AS blood_qty
            FROM bb_blood_group BG
            LEFT JOIN bb_
            (SELECT BS.*,BT.bloodstocktypename2,DN.hn,DN.donation_get_type_id,RH.rhname3,RH.rhcode
            FROM bb_bloodstock BS
            LEFT JOIN bb_donate DN ON BS.donateid = DN.donateid
            LEFT JOIN bb_bloodstock_type BT ON BS.bloodtype = BT.bloodstocktypeid
            LEFT JOIN bb_rh RH ON BS.bloodrh = RH.rhid
            WHERE DN.hn = '$hn'
            AND DN.donation_get_type_id = 2
            AND BS.bloodstockstatusid = 1
            AND BT.bloodstocktypegroupid = 1
            AND BS.active <> 0) M ON BG.bloodgroupid = M.bloodgroup
            WHERE BG.bloodgroupid NOT IN ('A','B','O','AB')
            GROUP BY BG.bloodgroupid
            HAVING blood_qty > 0
            
            ORDER BY FIELD(bloodgroupname, 'A', 'B', 'O', 'AB')
            ";
 
    $query = oci_parse($conn,$sql);
    oci_execute($query);


    $resultArray = array();
    if (mysqli_num_rows($query) > 0) {
        while($result = oci_fetch_array($query))
        {
            array_push($resultArray,$result);
        }
    }

    
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>