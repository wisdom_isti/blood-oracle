<?php
    include('../../connection.php');

    $id =$_GET['id'];
  
    $sql = "SELECT NVL(BBR.\"borrowbloodgroup\",'') AS borrowbloodgroup,
        NVL(BBR.\"borrowrh\",'') AS borrowrh,
        BBR.*,
        RT.\"receivingtypename2\",
        HT.\"hospitalname\",
        PT.\"patientfullname\",
        PT.\"patientgender\",
        PT.\"patientage\",
        PT.\"patientbloodgroup\",
        BBU.\"bloodborrowurgencyname\",
        BD.\"blooddeliveryname\",
        DOC.\"doctorname\"

        FROM \"bb_blood_borrow\" BBR
        LEFT JOIN \"bb_receiving_type\" RT ON BBR.\"receivingtypeid\" = RT.\"receivingtypeid\"
        LEFT JOIN \"bb_hospital\" HT ON BBR.\"hospitalid\" = HT.\"hospitalid\"
        LEFT JOIN \"bb_patient\" PT ON BBR.\"bloodborrowhn\" = PT.\"patienthn\"
        LEFT JOIN \"bb_blood_borrow_urgency\" BBU ON BBR.\"bloodborrowurgencyid\" = BBU.\"bloodborrowurgencyid\"
        LEFT JOIN \"bb_blood_delivery\" BD ON BBR.\"blooddeliveryid\" = BD.\"blooddeliveryid\"
        LEFT JOIN \"bb_doctor\" DOC ON BBR.\"bloodborrowdoctorid\" = DOC.\"doctorid\"
        WHERE BBR.\"bloodborrowid\" = '$id'
        ORDER BY BBR.\"bloodborrowid\" DESC";
 
    error_log($sql);

    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
    }

    $dataItem = array();
    foreach ($resultArray as $item) {
        array_push($dataItem, array_merge($item,getItem($item['bloodborrowid'])));
    }
    
    echo json_encode(
        array(
            'status' => true,
            'data' => $dataItem
        )
        
    );

    oci_close($conn);

    function getItem($id)
    {
        include('../../connection.php');

        $sql = "SELECT IM.* ,
                TY.\"bloodstocktypename2\"
                FROM \"bb_blood_borrow_item\" IM
                LEFT JOIN \"bb_bloodstock_type\" TY ON IM.\"bloodstocktypeid\" = TY.\"bloodstocktypeid\"
                WHERE IM.\"bloodborrowid\" = '$id'";

        $query = oci_parse($conn,$sql);
    oci_execute($query);

        $resultArray = array();
        while($result = oci_fetch_array($query))
        {
            array_push($resultArray,$result);
        }
  
        return   array(
                'item' => $resultArray
        );   

    }
?>