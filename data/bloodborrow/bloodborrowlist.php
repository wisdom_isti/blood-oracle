<?php
    include('../../connection.php');
    include('../pagination.php');

    $condition = '';
    $select = "BBR.*,
                RT.\"receivingtypename2\",
                HT.\"hospitalname\",
                PT.\"patientfullname\" ";
    $selectcount = " count(*) countpage ";
    
    $activePage = $_GET['activepage'];
    $numRows = $_GET['numrows'];

    $fromdate = $_GET['fromdate'];
    $todate = $_GET['todate'];
    $bloodborrowhn = $_GET['bloodborrowhn'];
    $receivingtypeid = $_GET['receivingtypeid'];
    $hospitalid = $_GET['hospitalid'];

    if(!empty($fromdate) && !empty($todate))
    $condition = $condition." AND BBR.\"bloodborrowdate\" BETWEEN TO_DATE ('$fromdate', 'YYYY-mm-dd')
			AND TO_DATE('$todate', 'YYYY-mm-dd') ";
   

    if(!empty($bloodborrowhn) )
    $condition = $condition." AND BBR.\"bloodborrowhn\" = '$bloodborrowhn' ";

    if(!empty($receivingtypeid) && $receivingtypeid != 'null')
    $condition = $condition." AND BBR.\"receivingtypeid\" = '$receivingtypeid' ";

    if(!empty($hospitalid) && $hospitalid != 'null')
    $condition = $condition." AND BBR.\"hospitalid\" = '$hospitalid' ";

    $sqlcount = condition($selectcount,$condition);
    
    $querycount = mysqli_query($conn,$sqlcount);


    $resultcount = oci_fetch_array($querycount);
    $pagination = paginationCompress(intval($resultcount['countpage']),$activePage,$numRows);

    $start = $pagination['start'];
    $numrow = $pagination['num_rows'];
    
    $sql = condition($select,$condition);
    $sql = $sql." LIMIT $start,$numrow ";
    $query = oci_parse($conn,$sql);
    oci_execute($query);
    
    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
    }

    
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'pagination' => $pagination
        )
        
    );

    oci_close($conn);

    function condition($select,$condition)
    {
        return "SELECT $select
    FROM \"bb_blood_borrow\" BBR
    LEFT JOIN \"bb_receiving_type\" RT ON BBR.\"receivingtypeid\" = RT.\"receivingtypeid\"
    LEFT JOIN \"bb_hospital\" HT ON BBR.\"hospitalid\" = HT.\"hospitalid\"
    LEFT JOIN \"bb_patient\" PT ON BBR.\"bloodborrowhn\" = PT.\"patienthn\"
    WHERE 1=1 $condition
    ORDER BY BBR.\"bloodborrowid\" DESC
    ";
    }


?>