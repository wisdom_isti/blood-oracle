<?php
    include('../../connection.php');

    $condition = '';


    $sql = "SELECT PT.* ,RH.\"rhname3\"
            FROM \"bb_patient\" PT
            LEFT JOIN \"bb_rh\" RH ON PT.\"patientrh\" = RH.\"rhid\"
            WHERE ROWNUM <= 500
            ORDER BY \"patientid\" DESC
            ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>