<?php
    include('../../connection.php');

    $condition = '';
    $bag_number =$_GET['bag_number'];
    $receivingtypeid = $_GET['receivingtypeid'];
    $hospitalid =$_GET['hospitalid'];

    $sql = "SELECT
            BR.\"receivingtypeid\",
            BR.\"hospitalid\",
            IM.\"bloodstocktypeid\",
            BR.* ,
            IM.\"bloodborrowitemid\",
            IM.\"bloodstocktypeid\",
            ST.\"bloodstocktypename2\",
            NVL(IM.\"a_qty\",0) AS \"a_qty\",
            NVL(IM.\"b_qty\",0) AS \"b_qty\",
            NVL(IM.\"o_qty\",0) AS \"o_qty\",
            NVL(IM.\"ab_qty\",0) AS \"ab_qty\",

            NVL(IM.\"cryo_qty\",0) AS \"cryo_qty\",

            NVL(IM.\"a_qty_get\",0) AS \"a_qty_get\",
            NVL(IM.\"b_qty_get\",0) AS \"b_qty_get\",
            NVL(IM.\"o_qty_get\",0) AS \"o_qty_get\",
            NVL(IM.\"ab_qty_get\",0) AS \"ab_qty_get\",
            NVL(IM.\"cryo_qty_get\",0) AS \"cryo_qty_get\"
            FROM \"bb_blood_borrow\" BR
            LEFT JOIN \"bb_blood_borrow_item\" IM ON BR.\"bloodborrowid\" = IM.\"bloodborrowid\"
            LEFT JOIN \"bb_bloodstock_type\" ST ON IM.\"bloodstocktypeid\" = ST.\"bloodstocktypeid\"
            WHERE BR.\"receivingtypeid\" = '$receivingtypeid'
            AND BR.\"hospitalid\" = '$hospitalid'
            AND NVL(IM.\"bloodstocktypeid\",'') IS NOT NULL
            AND ROWNUM <= 200 
            ORDER BY BR.\"bloodborrowid\" DESC
    
        ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>