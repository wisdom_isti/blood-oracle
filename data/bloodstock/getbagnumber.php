<?php
    include('../../connection.php');

    $condition = '';
    $bag_number =$_GET['bag_number'];
    $bloodtype =$_GET['bloodtype'];

    $sql = "SELECT * FROM \"bb_bloodstock_type\" WHERE \"bloodstocktypeid\" = '$bloodtype'"; 
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $bloodstocktypegroupid = '';
	while($result = oci_fetch_array($query))
	{
        $bloodstocktypegroupid = $result["bloodstocktypegroupid"];
    }


    $sql = "SELECT
            TO_DATE(TO_CHAR(SK.\"bloodexp\",'YYYY-MM-DD'),'YYYY-MM-DD') - TO_DATE((SELECT TO_CHAR(SYSDATE,'YYYY-MM-DD','NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') FROM DUAL), 'YYYY-MM-DD') AS \"statusexp\",
            SK.* ,
            RH.\"rhcode\",
            RH.\"rhname3\",
            TY.\"bloodstocktypegroupid\"
            FROM \"bb_bloodstock\" SK
            LEFT JOIN \"bb_rh\" RH ON SK.\"bloodrh\" = RH.\"rhid\"
            LEFT JOIN \"bb_bloodstock_type\" TY ON SK.\"bloodtype\" = TY.\"bloodstocktypeid\"
            WHERE SK.\"bloodstockstatusid\" = 1
            AND SK.\"bag_number\" = '$bag_number'
            WHERE SK.\"active\" <> 0
     ";
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
        if($bloodstocktypegroupid == $result["bloodstocktypegroupid"])
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
    );

    oci_close($conn);
?>