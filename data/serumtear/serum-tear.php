<?php
    include('../../connection.php');

    $condition = '';
    $hn =$_GET['hn'];

    $sql = "SELECT 	ST.* ,
                UO.unitofficename,
                DT.doctorname,
                VOL.serumtearvolumename,
                PT.patientfullname,
                STF.name AS staffname
                
            FROM bb_serum_tear ST
            LEFT JOIN bb_patient PT ON ST.patientid = PT.patientid
            LEFT JOIN bb_unit_office UO ON ST.unitofficeid = UO.unitofficeid
            LEFT JOIN bb_doctor DT ON ST.doctorid = DT.doctorid
            LEFT JOIN bb_staff STF ON ST.staffid = STF.id
            LEFT JOIN bb_serum_tear_volume VOL ON ST.serumtearvolumeid = VOL.serumtearvolumeid
            WHERE ST.active <> 0
            AND PT.patienthn = '$hn'
            ORDER BY ST.serumtearid DESC";
 
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray
        )
        
    );

    oci_close($conn);
?>