<?php
    include('../connection.php');

    $condition = '';
    $fromdate =$_GET['fromdate'];
    $todate =$_GET['todate'];

    $sql = "SELECT TO_CHAR(M.\"donation_date\" , 'dd-mm-yyyy') AS \"donation_date\" , M.\"bag_number\" , M.\"bloodtype\" , M.\"remark\"
        FROM
        (
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'PRC' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"prcremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"prc\",0) > 0
        AND \"prcused\" = 0
        AND \"prcinfect\" = 0
        AND \"prcisremark\" != 0
        
        UNION
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'LPRC' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"lprcremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"lprc\",0) > 0
        AND \"lprcused\" = 0
        AND \"lprcinfect\" = 0
        AND \"lprcisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'FFP' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"ffpremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"ffp\",0) > 0
        AND \"ffpused\" = 0
        AND \"ffpinfect\" = 0
        AND \"ffpisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'PC' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"pcremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"pc\",0) > 0
        AND \"pcused\" = 0
        AND \"pcinfect\" = 0
        AND \"pcisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'LPPC' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"lppcremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"lppc\",0) > 0
        AND \"lppcused\" = 0
        AND \"lppcinfect\" = 0
        AND \"lppcisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'LPPC(PAS)' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"lppc_pasremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"lppc_pas\",0) > 0
        AND \"lppc_pasused\" = 0
        AND \"lppc_pasinfect\" = 0
        AND \"lppc_pasisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'SDP' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"sdpremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"sdp\",0) > 0
        AND \"sdpused\" = 0
        AND \"sdpinfect\" = 0
        AND \"sdpisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'SDP(PAS)' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"sdp_pasremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"sdp_pas\",0) > 0
        AND \"sdp_pasused\" = 0
        AND \"sdp_pasinfect\" = 0
        AND \"sdp_pasisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'WB' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"wbremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"wb\",0) > 0
        AND \"wbused\" = 0
        AND \"wbinfect\" = 0
        AND \"wbisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'LDPRC' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"ldprcremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"ldprc\",0) > 0
        AND \"ldprcused\" = 0
        AND \"ldprcinfect\" = 0
        AND \"ldprcisremark\" != 0
        
        UNION 
        
        SELECT DN.\"donation_date\" , DN.\"bag_number\" , 'SDR' AS \"bloodtype\" , BR.\"bloodremarktext\" AS \"remark\"
        FROM \"bb_donate\" DN
        LEFT JOIN \"bb_blood_remark\" BR ON DN.\"sdrremark\" = BR.\"bloodremarkid\" 
        WHERE DN.\"donation_date\" BETWEEN TO_DATE('$fromdate' , 'yyyy/mm/dd') AND TO_DATE('$todate' , 'yyyy/mm/dd')
        AND NVL(DN.\"sdr\",0) > 0
        AND \"sdrused\" = 0
        AND \"sdrinfect\" = 0
        AND \"sdrisremark\" != 0
        ) \"M\"
        ORDER BY \"donation_date\" 

    ";
        $query = oci_parse($conn,$sql);
    oci_execute($query);

        $resultArray = array();
        while($result = oci_fetch_array($query))
        {
            array_push($resultArray,$result);
        }
        echo json_encode(
            array(
                'status' => true,
                'data' => $resultArray,
                'sql' => $sql
            )
            
        );
    
        oci_close($conn);
    ?>