<?php
    include('../connection.php');

    $condition = '';
    $fromdatetime =$_GET['fromdatetime'];
    $todatetime =$_GET['todatetime'];
    $usercreate =$_GET['usercreate'];

    if(!empty($usercreate))
    $condition = " AND BLL.usercreate = '$usercreate' ";

    $sql = "SELECT 
    ROW_NUMBER() OVER (ORDER By BLL.bloodlettingid DESC) AS num_row ,
    	BLL.* ,
                UO.unitofficename,
                DT.doctorname,
                BT.bagtypename,
                PT.patientfullname,
                PT.patienthn,
                BTP.lettingproblemname,
                DATE_FORMAT(BLL.bloodlettingdatetime,'%d/%m/%Y') AS bloodlettingdatetime,
                CONCAT(S.name,' ',S.surname) AS usercreatename ,
                DATE_FORMAT(BLL.bloodlettingdatetime,'%d/%m/%Y') AS datetimee  ,
                DATE_FORMAT(BLL.bloodlettingdatetime,'%H:%I') AS timeedate  
            FROM bb_blood_letting BLL
            LEFT JOIN bb_patient PT ON BLL.patientid = PT.patientid
            LEFT JOIN bb_unit_office UO ON BLL.unitofficeid = UO.unitofficeid
            LEFT JOIN bb_doctor DT ON BLL.doctorid = DT.doctorid
            LEFT JOIN bb_bag_type BT ON BLL.bagtypeid = BT.bagtypeid
            LEFT JOIN bb_blood_letting_problems BTP ON BLL.lettingproblemid = BTP.lettingproblemid
            LEFT JOIN bb_staff S ON BLL.usercreate = S.id
            WHERE BLL.active <> 0
            AND BLL.bloodlettingdatetime BETWEEN '$fromdatetime' AND '$todatetime'
            $condition
            ORDER BY BLL.bloodlettingid DESC";
 
    
    $query = oci_parse($conn,$sql);
    oci_execute($query);

    $resultArray = array();
	while($result = oci_fetch_array($query))
	{
		array_push($resultArray,$result);
	}
    echo json_encode(
        array(
            'status' => true,
            'data' => $resultArray,
            'sql' => $sql
        )
        
    );

    oci_close($conn);
?>