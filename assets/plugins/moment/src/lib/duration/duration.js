// Side effect imports
import './prototype';

import { createDuration } FROM bb_'./create';
import { isDuration } FROM bb_'./constructor';
import { getSetRelativeTimeThreshold } FROM bb_'./humanize';

export {
    createDuration,
    isDuration,
    getSetRelativeTimeThreshold
};
