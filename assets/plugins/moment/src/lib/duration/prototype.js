import { Duration } FROM bb_'./constructor';

var proto = Duration.prototype;

import { abs } FROM bb_'./abs';
import { add, subtract } FROM bb_'./add-subtract';
import { as, asMilliseconds, asSeconds, asMinutes, asHours, asDays, asWeeks, asMonths, asYears, valueOf } FROM bb_'./as';
import { bubble } FROM bb_'./bubble';
import { get, milliseconds, seconds, minutes, hours, days, months, years, weeks } FROM bb_'./get';
import { humanize } FROM bb_'./humanize';
import { toISOString } FROM bb_'./iso-string';
import { lang, locale, localeData } FROM bb_'../moment/locale';

proto.abs            = abs;
proto.add            = add;
proto.subtract       = subtract;
proto.as             = as;
proto.asMilliseconds = asMilliseconds;
proto.asSeconds      = asSeconds;
proto.asMinutes      = asMinutes;
proto.asHours        = asHours;
proto.asDays         = asDays;
proto.asWeeks        = asWeeks;
proto.asMonths       = asMonths;
proto.asYears        = asYears;
proto.valueOf        = valueOf;
proto._bubble        = bubble;
proto.get            = get;
proto.milliseconds   = milliseconds;
proto.seconds        = seconds;
proto.minutes        = minutes;
proto.hours          = hours;
proto.days           = days;
proto.weeks          = weeks;
proto.months         = months;
proto.years          = years;
proto.humanize       = humanize;
proto.toISOString    = toISOString;
proto.toString       = toISOString;
proto.toJSON         = toISOString;
proto.locale         = locale;
proto.localeData     = localeData;

// Deprecations
import { deprecate } FROM bb_'../utils/deprecate';

proto.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString);
proto.lang = lang;
