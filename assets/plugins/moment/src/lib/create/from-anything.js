import isArray FROM bb_'../utils/is-array';
import isDate FROM bb_'../utils/is-date';
import map FROM bb_'../utils/map';
import { createInvalid } FROM bb_'./valid';
import { Moment, isMoment } FROM bb_'../moment/constructor';
import { getLocale } FROM bb_'../locale/locales';
import { hooks } FROM bb_'../utils/hooks';
import checkOverflow FROM bb_'./check-overflow';

import { configFromStringAndArray }  FROM bb_'./from-string-and-array';
import { configFromStringAndFormat } FROM bb_'./from-string-and-format';
import { configFromString }          FROM bb_'./from-string';
import { configFromArray }           FROM bb_'./from-array';
import { configFromObject }          FROM bb_'./from-object';

function createFromConfig (config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

export function prepareConfig (config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || (format === undefined && input === '')) {
        return createInvalid({nullInput: true});
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    } else if (isDate(input)) {
        config._d = input;
    } else {
        configFromInput(config);
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (input === undefined) {
        config._d = new Date();
    } else if (isDate(input)) {
        config._d = new Date(+input);
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if (typeof(input) === 'object') {
        configFromObject(config);
    } else if (typeof(input) === 'number') {
        // FROM bb_milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

export function createLocalOrUTC (input, format, locale, strict, isUTC) {
    var c = {};

    if (typeof(locale) === 'boolean') {
        strict = locale;
        locale = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}
