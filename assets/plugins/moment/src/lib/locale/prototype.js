import { Locale } FROM bb_'./constructor';

var proto = Locale.prototype;

import { defaultCalendar, calendar } FROM bb_'./calendar';
import { defaultLongDateFormat, longDateFormat } FROM bb_'./formats';
import { defaultInvalidDate, invalidDate } FROM bb_'./invalid';
import { defaultOrdinal, ordinal, defaultOrdinalParse } FROM bb_'./ordinal';
import { preParsePostFormat } FROM bb_'./pre-post-format';
import { defaultRelativeTime, relativeTime, pastFuture } FROM bb_'./relative';
import { set } FROM bb_'./set';

proto._calendar       = defaultCalendar;
proto.calendar        = calendar;
proto._longDateFormat = defaultLongDateFormat;
proto.longDateFormat  = longDateFormat;
proto._invalidDate    = defaultInvalidDate;
proto.invalidDate     = invalidDate;
proto._ordinal        = defaultOrdinal;
proto.ordinal         = ordinal;
proto._ordinalParse   = defaultOrdinalParse;
proto.preparse        = preParsePostFormat;
proto.postformat      = preParsePostFormat;
proto._relativeTime   = defaultRelativeTime;
proto.relativeTime    = relativeTime;
proto.pastFuture      = pastFuture;
proto.set             = set;

// Month
import {
    localeMonthsParse,
    defaultLocaleMonths,      localeMonths,
    defaultLocaleMonthsShort, localeMonthsShort
} FROM bb_'../units/month';

proto.months       =        localeMonths;
proto._months      = defaultLocaleMonths;
proto.monthsShort  =        localeMonthsShort;
proto._monthsShort = defaultLocaleMonthsShort;
proto.monthsParse  =        localeMonthsParse;

// Week
import { localeWeek, defaultLocaleWeek, localeFirstDayOfYear, localeFirstDayOfWeek } FROM bb_'../units/week';
proto.week = localeWeek;
proto._week = defaultLocaleWeek;
proto.firstDayOfYear = localeFirstDayOfYear;
proto.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
import {
    localeWeekdaysParse,
    defaultLocaleWeekdays,      localeWeekdays,
    defaultLocaleWeekdaysMin,   localeWeekdaysMin,
    defaultLocaleWeekdaysShort, localeWeekdaysShort
} FROM bb_'../units/day-of-week';

proto.weekdays       =        localeWeekdays;
proto._weekdays      = defaultLocaleWeekdays;
proto.weekdaysMin    =        localeWeekdaysMin;
proto._weekdaysMin   = defaultLocaleWeekdaysMin;
proto.weekdaysShort  =        localeWeekdaysShort;
proto._weekdaysShort = defaultLocaleWeekdaysShort;
proto.weekdaysParse  =        localeWeekdaysParse;

// Hours
import { localeIsPM, defaultLocaleMeridiemParse, localeMeridiem } FROM bb_'../units/hour';

proto.isPM = localeIsPM;
proto._meridiemParse = defaultLocaleMeridiemParse;
proto.meridiem = localeMeridiem;
