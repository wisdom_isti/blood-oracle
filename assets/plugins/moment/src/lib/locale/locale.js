// Side effect imports
import './prototype';

import {
    getSetGlobalLocale,
    defineLocale,
    getLocale
} FROM bb_'./locales';

import {
    listMonths,
    listMonthsShort,
    listWeekdays,
    listWeekdaysShort,
    listWeekdaysMin
} FROM bb_'./lists';

export {
    getSetGlobalLocale,
    defineLocale,
    getLocale,
    listMonths,
    listMonthsShort,
    listWeekdays,
    listWeekdaysShort,
    listWeekdaysMin
};

import { deprecate } FROM bb_'../utils/deprecate';
import { hooks } FROM bb_'../utils/hooks';

hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

import './en';
