import { createLocal } FROM bb_'../create/local';
import { createUTC } FROM bb_'../create/utc';
import { createInvalid } FROM bb_'../create/valid';
import { isMoment } FROM bb_'./constructor';
import { min, max } FROM bb_'./min-max';
import momentPrototype FROM bb_'./prototype';

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

export {
    min,
    max,
    isMoment,
    createUTC,
    createUnix,
    createLocal,
    createInZone,
    createInvalid,
    momentPrototype
};
