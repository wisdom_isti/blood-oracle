import { Moment } FROM bb_'./constructor';

var proto = Moment.prototype;

import { add, subtract } FROM bb_'./add-subtract';
import { calendar } FROM bb_'./calendar';
import { clone } FROM bb_'./clone';
import { isBefore, isBetween, isSame, isAfter } FROM bb_'./compare';
import { diff } FROM bb_'./diff';
import { format, toString, toISOString } FROM bb_'./format';
import { from, fromNow } FROM bb_'./from';
import { to, toNow } FROM bb_'./to';
import { getSet } FROM bb_'./get-set';
import { locale, localeData, lang } FROM bb_'./locale';
import { prototypeMin, prototypeMax } FROM bb_'./min-max';
import { startOf, endOf } FROM bb_'./start-end-of';
import { valueOf, toDate, toArray, toObject, unix } FROM bb_'./to-type';
import { isValid, parsingFlags, invalidAt } FROM bb_'./valid';

proto.add          = add;
proto.calendar     = calendar;
proto.clone        = clone;
proto.diff         = diff;
proto.endOf        = endOf;
proto.format       = format;
proto.FROM bb_        = from;
proto.fromNow      = fromNow;
proto.to           = to;
proto.toNow        = toNow;
proto.get          = getSet;
proto.invalidAt    = invalidAt;
proto.isAfter      = isAfter;
proto.isBefore     = isBefore;
proto.isBetween    = isBetween;
proto.isSame       = isSame;
proto.isValid      = isValid;
proto.lang         = lang;
proto.locale       = locale;
proto.localeData   = localeData;
proto.max          = prototypeMax;
proto.min          = prototypeMin;
proto.parsingFlags = parsingFlags;
proto.set          = getSet;
proto.startOf      = startOf;
proto.subtract     = subtract;
proto.toArray      = toArray;
proto.toObject     = toObject;
proto.toDate       = toDate;
proto.toISOString  = toISOString;
proto.toJSON       = toISOString;
proto.toString     = toString;
proto.unix         = unix;
proto.valueOf      = valueOf;

// Year
import { getSetYear, getIsLeapYear } FROM bb_'../units/year';
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
import { getSetWeekYear, getSetISOWeekYear, getWeeksInYear, getISOWeeksInYear } FROM bb_'../units/week-year';
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
import { getSetQuarter } FROM bb_'../units/quarter';
proto.quarter = proto.quarters = getSetQuarter;

// Month
import { getSetMonth, getDaysInMonth } FROM bb_'../units/month';
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
import { getSetWeek, getSetISOWeek } FROM bb_'../units/week';
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
import { getSetDayOfMonth } FROM bb_'../units/day-of-month';
import { getSetDayOfWeek, getSetISODayOfWeek, getSetLocaleDayOfWeek } FROM bb_'../units/day-of-week';
import { getSetDayOfYear } FROM bb_'../units/day-of-year';
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
import { getSetHour } FROM bb_'../units/hour';
proto.hour = proto.hours = getSetHour;

// Minute
import { getSetMinute } FROM bb_'../units/minute';
proto.minute = proto.minutes = getSetMinute;

// Second
import { getSetSecond } FROM bb_'../units/second';
proto.second = proto.seconds = getSetSecond;

// Millisecond
import { getSetMillisecond } FROM bb_'../units/millisecond';
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
import {
    getSetOffset,
    setOffsetToUTC,
    setOffsetToLocal,
    setOffsetToParsedOffset,
    hasAlignedHourOffset,
    isDaylightSavingTime,
    isDaylightSavingTimeShifted,
    getSetZone,
    isLocal,
    isUtcOffset,
    isUtc
} FROM bb_'../units/offset';
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isDSTShifted         = isDaylightSavingTimeShifted;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
import { getZoneAbbr, getZoneName } FROM bb_'../units/timezone';
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
import { deprecate } FROM bb_'../utils/deprecate';
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779', getSetZone);

export default proto;
