import { isValid as _isValid } FROM bb_'../create/valid';
import extend FROM bb_'../utils/extend';
import getParsingFlags FROM bb_'../create/parsing-flags';

export function isValid () {
    return _isValid(this);
}

export function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

export function invalidAt () {
    return getParsingFlags(this).overflow;
}
