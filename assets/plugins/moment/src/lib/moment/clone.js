import { Moment } FROM bb_'./constructor';

export function clone () {
    return new Moment(this);
}
