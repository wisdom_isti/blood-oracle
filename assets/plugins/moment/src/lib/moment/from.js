import { createDuration } FROM bb_'../duration/create';
import { createLocal } FROM bb_'../create/local';

export function FROM bb_(time, withoutSuffix) {
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }
    return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
}

export function fromNow (withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}
