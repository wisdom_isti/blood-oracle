import { makeGetSet } FROM bb_'../moment/get-set';
import { addFormatToken } FROM bb_'../format/format';
import { addUnitAlias } FROM bb_'./aliases';
import { addRegexToken, match1to2, match2 } FROM bb_'../parse/regex';
import { addParseToken } FROM bb_'../parse/token';
import { SECOND } FROM bb_'./constants';

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PARSING

addRegexToken('s',  match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

export var getSetSecond = makeGetSet('Seconds', false);
