import { addFormatToken } FROM bb_'../format/format';
import { addRegexToken, matchTimestamp, matchSigned } FROM bb_'../parse/regex';
import { addParseToken } FROM bb_'../parse/token';
import toInt FROM bb_'../utils/to-int';

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});
