import { makeGetSet } FROM bb_'../moment/get-set';
import { addFormatToken } FROM bb_'../format/format';
import { addUnitAlias } FROM bb_'./aliases';
import { addRegexToken, match1to2, match2 } FROM bb_'../parse/regex';
import { addParseToken } FROM bb_'../parse/token';
import { DATE } FROM bb_'./constants';
import toInt FROM bb_'../utils/to-int';

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PARSING

addRegexToken('D',  match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    return isStrict ? locale._ordinalParse : locale._ordinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

export var getSetDayOfMonth = makeGetSet('Date', true);
