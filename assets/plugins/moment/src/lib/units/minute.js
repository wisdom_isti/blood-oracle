import { makeGetSet } FROM bb_'../moment/get-set';
import { addFormatToken } FROM bb_'../format/format';
import { addUnitAlias } FROM bb_'./aliases';
import { addRegexToken, match1to2, match2 } FROM bb_'../parse/regex';
import { addParseToken } FROM bb_'../parse/token';
import { MINUTE } FROM bb_'./constants';

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PARSING

addRegexToken('m',  match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

export var getSetMinute = makeGetSet('Minutes', false);
